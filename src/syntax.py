# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "savinov"
__date__ = "$Apr 23, 2015 10:42:09 PM$"

import os
import collections
import tokens

def readTable():
    lines = []
    with open(sourceFileName, 'r') as sourceFile:
        for line in sourceFile:
            if("-".find(line)>=0):
                continue;
            if("|".find(line) >= 0):
                lines.append(line)
                pass
    sourceFile.close()
    return lines
    pass

def deleteAllSpace(line):
    out = "";
    for c in line:
        if not tokens.isSpace(c) :
            out += c
        pass
    return out

def addToTable(rulesTable, tokensArray, curToken, value, index):
    token = tokensArray[index]
    value = deleteAllSpace(value)
    i = ",".find(value)
    tok = value[0, i]
    ruleNum = value[i+1, len(value)]
    rulesTable[token][curToken] = [tok, int(num)]
    pass

def addToTableFirstLine(rulesTable, tokensArray, token, index):
    if index==0: return
    token = deleteAllSpace()
    if token == "":
        return
    tokensArray.append(token)
    rulesTable[token] = collections.OrderedDict()
    pass

def parceLine(line, i):
    firstStep = 1;
    start = 0
    end = 0
    cols = -1
    curToken = ""
    while true:
        end = "|".find(line, start)
        if end < 0 :
            break;
        pass
    pass

def parceTable(lines):
    rulesTable = collections.OrderedDict()
    for i in range(0, len(lines)):
        line = lines[i]
        parceLine(line)
    pass


