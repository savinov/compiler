# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = ""
__date__ = "$Mar 28, 2015 11:23:11 AM$"

#from optparse import OptionParser
import collections
import getopt
import os
import sys

NAME_TOKEN_ID = "ID"
NAME_TOKEN_INT = "CI"
NAME_TOKEN_FLOAT = "CR"
#CI 1, 1000, -20
#CR 1.15
TOKEN_ID = 1;
TOKEN_NUMBER = 2;
TOKEN_KNOWN = 3;
TOKEN_SPACE = 4;


codeToTokenIndex = {"++": "UAOP1", "--": "UAOP2", "+":  "AAOP1", "-":  "AAOP2"
    , "*":  "MAOP"
    , "/":  "UAOP"
    , "!":  "NLOP"
    , "||": "OLOP"
    , "&&": "ALOP"
    , "^":  "EOLOP"

    , "<":  "RLOP1", ">":  "RLOP2", "<=":  "RLOP3", ">=":  "RLOP4"
    , "==": "EQLOP1", "!=": "EQLOP2"
    , "=":  "AO1", "*=":  "AO2", "+=":  "AO3", "-=":  "AO4", "/=":  "AO5"
    , "<<": "IOOP1", ">>": "IOOP2"
    , "(": "SRLP"
    , ")": "SRRP"
    , "{": "SRLCB"
    , "}": "SRRCB"
    , ";": "SRSM"
    , ".": "SRPT"
    , ",": "SRCA"
    , "?": "SRQN"
    , ":": "SRCN"
    , "\n": ""

    , "if": "KWIF"
    , "else": "KWELSE"
    , "void": "KWV"

    , "int": "KWI"    
    , "float":"KWFL"
    , "Matrix": "KWM"    
    , "main": "KWMN"   
    , "add": "KWADD"  
    , "sub": "KWSUB"  
    , "mul": "KWMUL"  
    , "trans":"KWTS"   
    , "rows": "KWRW"   
    , "cols": "KWCL"   
    , "at": "KWAT"   
    , "do": "KWDO"   
    , "while":"KWWL"   
    , "enum": "KWENUM" 
    , "const":"KWCONST"
    }

tokenToCodeIndex = {}

typesTable = collections.OrderedDict()
basicType = collections.namedtuple("BasicType", ["id", "sizeof"])
typesTable['int'] = basicType(0, 4)
typesTable['float'] = basicType(1, 4)
typesTable['Matrix'] = basicType(2, -1)
typesTable['void'] = basicType(3, -1)

type0 = collections.OrderedDict()#int
type1 = collections.OrderedDict()#float
type2 = collections.OrderedDict()#Matrix

idTable = collections.OrderedDict();
reverseIdTable = collections.OrderedDict();
constTable = collections.OrderedDict();

def flipKeyValue(map):
    out = {}
    for key in map:
        out[map[key]] = key
    return out;

def readSourceCode(sourceFileName):
    lines = []
    with open(sourceFileName, 'r') as sourceFile:
        for line in sourceFile:
            lines.append(line)
    sourceFile.close()
    return lines

def writeToFile(fileName, stringArray):
    with open(fileName, 'w') as sourceFile:
        for line in stringArray:
            sourceFile.write(line)
            sourceFile.write('\n')
    return sourceFile.close()

def isId(char):
    return char.isalpha()

def isNumber(char):
    return char.isdigit();

def isSpace(char):
    return char == ' ' or char == '\t';

def isNewLine(char):
    return char == '\n' or char == '\r';

def isSign(char):
    return char == '+' or char == '-' or char == '*' or char == '/' \
        or char == '!' or char == '|' or char == '&' or char == '='\
        or char == '^' or char == '<' or char == '>' or char == ';' \
        or char == '(' or char == ')' or char == '{' or char == '}'\
        or char == '.' or char == ':' or char == ',' or char == '?';

def getId(line, index):
    out = "";
    while(index[0] < len(line) and isId(line[index[0]]) or isNumber(line[index[0]])):
        out += line[index[0]]
        index[0] += 1;
    return out

def getNumber(line, index):
    out = "";
    while(index[0] < len(line) and isNumber(line[index[0]])):
        out += line[index[0]]
        index[0] += 1;
    return out

def getSign(line, index):
    out = "";
    while(index[0] < len(line) and isSign(line[index[0]])):
        if(out and out in codeToTokenIndex and not (out + line[index[0]] in codeToTokenIndex)):
            break
        out += line[index[0]]
        index[0] += 1;
    return out

def getSpace(line, index):
    out = "";
    while(index[0] < len(line) and isSpace(line[index[0]])):
        out += line[index[0]]
        index[0] += 1;
    return out

def getNewLine(line, index):
    out = "";
    while(index[0] < len(line) and isNewLine(line[index[0]])):
        out += line[index[0]]
        index[0] += 1;
    return out



def checkCharAndGet(line, index):
    char = line[index[0]]
    if(isNumber(char)):
        return getNumber(line, index)
    elif(isSign(char)):
        return getSign(line, index)
    elif (isSpace(char)):
        return getSpace(line, index)
    elif (isNewLine(char)):
        return getNewLine(line, index)
    return getId(line, index)
def detToken(lexem):
    result = collections.namedtuple("Result", ['result', 'token']);
    if(isSpace(lexem[0])):
        res = result(TOKEN_SPACE, lexem);
    elif(lexem in codeToTokenIndex):
        res = result(TOKEN_KNOWN, codeToTokenIndex[lexem]);
    else:
        allNumber = 1;
        for i in range(0, len(lexem)):
            if(not isNumber(lexem[i])):
                allNumber = 0;
                break;
        if(allNumber == 0):
            res = result(TOKEN_ID, lexem);
        else:
            res = result(TOKEN_NUMBER, lexem);
    pass
    return res

def tokenizeSourceCode(sourceCodeLines):
    tokenizeCode = [];
    idCounter = 0
    realCounter = 0
    intCounter = 0
    for i in range(0, len(sourceCodeLines)):
        sourceLine = sourceCodeLines[i]
        j = [0]
        j[0] = 0
        tokenizeCode.append("");
        tmpStr = ""
        lastType = -1
        reedMatrix = 0
        while(j[0] < len(sourceLine)):
            lexem = checkCharAndGet(sourceLine, j)
            res = detToken(lexem)
            token = ""
            if((res[0] == TOKEN_NUMBER and j[0] < len(sourceLine) and sourceLine[j[0]] == '.')):
                tmpStr = lexem;
                tmpStr += ".";
                lexem = ""
                j[0] += 1;
                pass
            elif(res[0] == TOKEN_KNOWN):
                if(lexem in typesTable):
                    lastType = typesTable[lexem][0]
                elif(not isSign(lexem[0])):
                    lastType = -1
                    pass
                if(lastType == typesTable['Matrix'][0] and lexem == '}'):
                    reedMatrix = 1
                    type2[idCounter - 1] = []
                tokenizeCode[i] += res[1] + " "
                pass
            elif(res[0] != TOKEN_SPACE):
                if(tmpStr and res[0] == TOKEN_NUMBER):
                    token = NAME_TOKEN_FLOAT + str(realCounter);
                    if(reedMatrix > 0):
                        type2[idCounter - 1].append(token);
                        if(len(type2[idCounter - 1]) == 2):
                            type2[idCounter - 1].append(0)
                            reedMatrix = 0;
                        pass
                    else:
                        type1[token] = tmpStr + lexem
                        pass
                    pass
                    realCounter += 1
                elif(res[0] == TOKEN_NUMBER):
                    token = NAME_TOKEN_INT + str(intCounter);
                    if(reedMatrix > 0):
                        type2[idCounter - 1].append(token);
                        if(len(type2[idCounter - 1]) == 2):
                            type2[idCounter - 1].append(0)
                            reedMatrix = 0;
                        pass
                    type0[token] = lexem
                    intCounter += 1
                elif(res[0] == TOKEN_ID):
                    value = collections.namedtuple("Id", ["name", "type"]);
                    if(lexem in reverseIdTable):
                        token = reverseIdTable[lexem]
                    elif(lastType != -1):
                        token = NAME_TOKEN_ID + str(idCounter)
                        idTable[token] = value(lexem, lastType);
                        reverseIdTable[lexem] = token
                        idCounter += 1
                    pass
                tmpStr = "";
                tokenizeCode[i] += token + " "
                pass
            pass
    return tokenizeCode
