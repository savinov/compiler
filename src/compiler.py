# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = ""
__date__ = "$Mar 28, 2015 11:23:11 AM$"

#from optparse import OptionParser
import collections
import sys
import os
import getopt
import tokens

def main(argv):
    build_dir = "../build"
    if(not os.path.exists(build_dir)):
        os.mkdirs(build_dir);
    file = ""
    try:
        opts, args = getopt.getopt(argv, "hf:", ["help", "file="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-f", "--file"):
            file = arg
            
    if(not file):
        print("file is empty");
        sys.exit(2);
    print("file:", file);
    sourceCode = tokens.readSourceCode(file)
    if(sourceCode):
        result = tokens.tokenizeSourceCode(sourceCode);
        tokens.writeToFile(build_dir + "/type_int_table_" + file, (str(k) + " " + str(v) for k, v in tokens.type0.items()))
        tokens.writeToFile(build_dir + "/type_float_table" + file, (str(k) + " " + str(v) for k, v in tokens.type1.items()))
        tokens.writeToFile(build_dir + "/type_matrix_table" + file, (str(k) + " " + str(v) for k, v in tokens.type2.items()))
        tokens.writeToFile(build_dir + "/types_table" + file, (str(k) + " " + str(v) for k, v in tokens.typesTable.items()))
        tokens.writeToFile(build_dir + "/id_table" + file, (str(k) + " " + str(v) for k, v in tokens.idTable.items()))
        tokens.writeToFile(build_dir + "/tokenized_" + file, result)
    else:
        print("sourceCode is empty");
    print("end");
    sys.exit(0)
        
if __name__ == "__main__":
    main(sys.argv[1:])
